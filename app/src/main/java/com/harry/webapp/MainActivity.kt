package com.harry.webapp

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import com.harry.webapp.common.AppConstents
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.Toast
import com.github.ybq.android.spinkit.SpinKitView
import com.harry.webapp.common.AppConstents.OPENKEYBORD_LINK
import com.harry.webapp.healperdialog.HelperDialog
import kotlinx.android.synthetic.main.loader.*
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.view.Window
import android.widget.Button
import android.widget.TextView


class MainActivity : AppCompatActivity() {
    lateinit  var webView  : WebView
    lateinit  var progress_bar  : SpinKitView
    var dialogFlag: Boolean = true
    lateinit var dialog : Dialog

    lateinit var helperDialog : HelperDialog ;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        webView = findViewById(R.id.webView)

        progress_bar = findViewById(R.id.progress_bar)

        helperDialog  =  HelperDialog(this)
       // helperDialog.showLoader()

        /*
        * Load WebView data from static link is define in AppConstents



        * */

        if(isConnected){

            loadWebView()
            progressBar()

        }else{

            noInternetConnection()



        }


    }

    private fun noInternetConnection() {


        val dialog = Dialog(this)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setCancelable(false)
        dialog .setContentView(R.layout.no_internet_dailog)

        dialog.show()

        val yesBtn = dialog .findViewById(R.id.yesBtn) as Button
        val noBtn = dialog .findViewById(R.id.noBtn) as TextView
        yesBtn.setOnClickListener {

            val callGPSSettingIntent = Intent(android.provider.Settings.ACTION_WIFI_SETTINGS)
           startActivityForResult(callGPSSettingIntent , 100)

        }
        noBtn.setOnClickListener {

            onDestroy()
        }





//        val builder = AlertDialog.Builder(this)
//        builder.setTitle("No Internet")
//        builder.setMessage("No Internet Please Check Connection")
//        //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))
//
//
//        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
//
//
//            val callGPSSettingIntent = Intent(android.provider.Settings.ACTION_WIFI_SETTINGS)
//            startActivityForResult(callGPSSettingIntent , 100)
//
//        }
//
//        builder.setNegativeButton(android.R.string.no) { dialog, which ->
//
//            onDestroy()
//
//        }
//
//
//        builder.show()


    }

    fun progressBar(){



        webView.webChromeClient = object: WebChromeClient(){
            override fun onProgressChanged(view: WebView, newProgress: Int) {


                if (newProgress == 100) {

                    progress_bar.visibility = View.GONE
//                    helperDialog.dismissLoader()
                    dialogFlag = false
                }
                else {

                    if (!dialogFlag){

                        progress_bar.visibility = View.GONE
                     //   helperDialog.showLoader()
                        dialogFlag = true
                    }

                }


            }
        }


        webView.webViewClient = object : WebViewClient() {


            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)


                Log.e("onPageStarted","URLLL" + url) ;

            }



            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {

                webView.loadUrl("file:///android_asset/error.html")
            }

            override fun onLoadResource(view: WebView, url: String) { //Doesn't work
//                swipe.setRefreshing(true)



            }

            override fun onPageFinished(view: WebView, url: String) {


                Log.e("onPageFinished","URLLL" + url) ;


            }

        }
    }


    fun loadWebView(){
        webView.loadUrl(AppConstents.App_URL_LINK);
        webView.settings.setJavaScriptEnabled(true);
        webView.settings.setSupportZoom(false);
    }

    override fun onBackPressed() {
        if (webView.canGoBack()){
            webView.goBack()



        }else{

            //super.onBackPressed()
            exitConfirmDailog() ;
        }
    }

    val Context.isConnected: Boolean
        get() {
            return (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
                .activeNetworkInfo?.isConnected == true
        }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 100){

            loadWebView()
            progressBar()
        }






    }




    private fun exitConfirmDailog() {


        val dialog = Dialog(this)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setCancelable(false)
        dialog .setContentView(R.layout.back_press_dialog)

        dialog.show()

        val confirm = dialog .findViewById(R.id.confirm) as Button
        val cancel = dialog .findViewById(R.id.cancel) as TextView
        confirm.setOnClickListener {



            onDestroy()

        }
        cancel.setOnClickListener {

            dialog.dismiss()
        }





//        val builder = AlertDialog.Builder(this)
//        builder.setTitle("No Internet")
//        builder.setMessage("No Internet Please Check Connection")
//        //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))
//
//
//        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
//
//
//            val callGPSSettingIntent = Intent(android.provider.Settings.ACTION_WIFI_SETTINGS)
//            startActivityForResult(callGPSSettingIntent , 100)
//
//        }
//
//        builder.setNegativeButton(android.R.string.no) { dialog, which ->
//
//            onDestroy()
//
//        }
//
//
//        builder.show()


    }


}
